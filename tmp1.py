import re
import json

with open('valid_zhuyin.txt') as f:
    valid_syllables = re.split('[\r\n,\s]+', f.read())

with open('header_list.json') as f:
    keys = [k for k in json.loads(f.read()).keys()]

ks = [k for k in valid_syllables if not k in keys]

print(ks)