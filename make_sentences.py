import json
import math
import operator
from copy import deepcopy

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Update 2018 / 05 / 28
# - - - - - - - - - - - - - - - - - - - - - - - - - - -

# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# semantic_path  : use log() to convert words frequency of syllables,
#                  and this lists can tell us the relations between
#                  words and syllables.
#                  word : { syllable_A: [ word_A: 4.5932 , word_B: 3.5687 ... ],
#                           syllable_B: [ word_C: 3.5689 , worc_D: 1.2681 ... ],
#                          ....  }
#
# word_freq_path : use log() to convert words frequency of syllables.
#                  syllable : {'a' : 2.235325 , 'b' : 3.56421 , ... }
#
# header_path    : use log() to convert header words frequency of syllables.
#                  syllable : {'a' : 2369 , 'b' : 1245 , ... }
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
semantic_path = 'word_log.json'
word_freq_path = 'freq_log.json'
header_path = 'header_list.json'


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Open input file.
# input_path : path of file
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def openFile(input_path):
    # open file
    input_json = open(input_path, 'r')
    # read json file
    json_lists = json.load(input_json)

    return json_lists


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Parse the words to pronunciations
# lists : input lists of Chinese words and frequency
# [older version]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def parseSentece(words):
    word_freq = openFile(word_freq_path)
    semantic = openFile(semantic_path)
    header = openFile(header_path)

    length = len(words)
    sentences = []

    word = header_parse(words[0], header)
    sentences.append(word)
    print(sentences[0])

    for i in range(1, length):
        next_word = parseWords(sentences[i - 1], words[i], semantic, word_freq)
        sentences.append(next_word)

    print(sentences)


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Parse the words to pronunciations
# lists : input lists of Chinese words and frequency
# [older version]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def parseWords(word, next_syllable, semantic, word_freq):
    _dict = semantic[word]

    count = -1
    need_word = ''

    if next_syllable in _dict:
        _dict_syll = _dict[next_syllable]
        _dict_words = _dict_syll[1]

        for word, freq in _dict_words.items():
            if count < freq:
                need_word = word
                count = freq

    else:
        word_temp = word_freq[next_syllable]
        need_word = word_temp[0][1]

    return need_word


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Find out the header words of all syllables.
# syllables : list of syllables
#             {'a':0.8532,'b':0.3236...}
# header    : path of header list
# [20180515 version]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def header_parse(syllables, header):
    chosen_word_count = 0
    chosen_header_word = ''

    # {'syllable_a':0.8532,'syllable_b':0.3236...}
    for syllable, syllable_pr in syllables.items():

        # { word1:562 , word2:263 , word3:123... }
        header_list = header[syllable]

        # find biggest frequency of word from same syllable
        need_header, count = max(header_list.items(), key=lambda it: it[1])

        # use log() to decrease difference
        count = math.log(count, 4) * syllable_pr
        print(need_header)
        print(count)
        # find biggest score of word from all syllables
        if count > chosen_word_count:
            chosen_header_word = need_header
            chosen_word_count = count

    return chosen_header_word, chosen_word_count


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Parse the words and next syllable.
# word          : the previous word
# next_syllable : the next syllable
# semantic      : the list of relation about word and syllable
# word_freq     : the list of frequency of word
# [20180528 version]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def parseWords_v1(word, next_syllable, semantic, word_freq):
    # semantic list
    if word in semantic:
        _dict = semantic[word]
    else:
        semantic.update({word: {}})
        _dict = {}

    # if word and syllable are exist in list
    _connect = False

    # initial parameter
    need_3_word = []
    need_3_score = []

    _top = 3
    # if word and syllable are exist
    if next_syllable in _dict:
        _dict_syll = _dict[next_syllable]
        _dict_words = _dict_syll[1]
        _connect = True

        # find out the top 3 score words
        sorted_dict_words = list(sorted(_dict_words.items(), key=lambda it: it[1],
                                        reverse=True))

        _size = len(sorted_dict_words)
        tmp_words = sorted_dict_words[:min(_top, _size)]

        # make the words and score list
        for word, log_score in tmp_words:
            need_3_word.append(word)
            need_3_score.append(log_score)

    # if not exist, we use words frequency list
    # to make a possible words.
    else:
        word_temp = word_freq[next_syllable]
        length_temp = len(word_temp)
        for i in range(min(_top, length_temp)):
            score, word = word_temp[i]
            need_3_word.append(word)
            need_3_score.append(score)

    return need_3_word, need_3_score, _connect


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Parse the sequence of syllables , this is the important
# function for creating sentence.
# words     : the list of syllables
#       [{syllable:score , syllable:score , syllable:score },
#        {syllable:score , syllable:score , syllable:score },
#                           ......
#        {syllable:score , syllable:score , syllable:score },
#        {syllable:score , syllable:score , syllable:score }]
# [20180528 version]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def parseSentece_v1(words):
    word_freq = openFile(word_freq_path)
    semantic = openFile(semantic_path)
    header = openFile(header_path)

    sentence_length = len(words)

    all_sentences = []

    # set the wanted num, long sentence needs few syllables to become fast.
    wanted_num = max(5, min(9, 14 - sentence_length))

    # find header word
    count_connect = 0
    word, score = header_parse(words[0], header)
    all_sentences.append([score, count_connect, word])

    # global connection count
    syllables_list = pick_top_syllable(words, wanted_num)
    all_sentences = make_possible_sentence(syllables_list, sentence_length,
                                           all_sentences, semantic, word_freq)
    best_possible_sentence = find_best_sentence(all_sentences)
    # delete count_connect attribute => [score, 'w1', 'w2', ..., 'wn']
    del best_possible_sentence[1]

    return best_possible_sentence


# [{syllable:score , syllable:score , syllable:score },
#  {syllable:score , syllable:score , syllable:score }]
def pick_top_syllable(list_of_syllables, wanted_num):
    picked_syllables = []
    for syllables in list_of_syllables:
        sorted_syllables = [{k: v} for k, v in
                            sorted(syllables.items(), reverse=True, key=operator.itemgetter(1))]
        picked_syllables.append(sorted_syllables[:wanted_num])
        # print(sorted_syllables)

    # print(picked_syllables)
    return picked_syllables


# find the best sentence
def find_best_sentence(all_sentences):
    return max(all_sentences, key=lambda s: s[0])  # s[0] is score


# [[{'s11': score11},{'s12': score12},{}],[{'s21': score21},{},{}]...]
def make_possible_sentence(syllables_ls, sentence_len, all_sentences_, semantic, word_freq):
    all_sentences = deepcopy(all_sentences_)
    # first word has parsed, so starts from second index 1
    for i in range(1, sentence_len):
        # To backup the results of the previous step
        all_stn = deepcopy(all_sentences)

        # reset sentences
        all_sentences.clear()

        for a_stn in all_stn:

            prev_score, prev_word = a_stn[0], a_stn[i + 1]
            syllables_l = syllables_ls[i]

            for pair in syllables_l:
                for syllable, prob in pair.items():
                    p_words, scores, is_connected = parseWords_v1(prev_word, syllable, semantic, word_freq)

                    # we can adjust number in range, the number can be 1, 2 or 3
                    max_consider_num = min(len(p_words), 1)  # 1, 2, 3
                    p_words, scores = p_words[:max_consider_num], scores[:max_consider_num]

                    for p_word, score_ in zip(p_words, scores):
                        if p_word == '':
                            continue
                        sentence = deepcopy(a_stn)
                        sentence.append(p_word)

                        # the score can be adjusted - - - - - -
                        score = prev_score + score_ * prob * 3

                        # bonus 08/04
                        count_ = sentence[1]
                        if is_connected:
                            if count_ != 2:
                                count_ += 1
                            score = score + math.pow(4, count_)
                        else:
                            count_ = 0
                        # ------------
                        sentence[0] = score
                        sentence[1] = count_
                        all_sentences.append(sentence)
                        # - - - - - - - - - - - - - - - - - - -

    return all_sentences
