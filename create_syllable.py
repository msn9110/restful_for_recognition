from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import label_wav as lw
import make_sentences as mks
import take_audio
import adjust_relation
import math

from pypinyin import pinyin, Style

# all the syllables
# all the syllables
with open('valid_zhuyin.txt') as f:
    not_in_header = ['ㄟ', '']
    valid_syllables = [k for k in f.read().split('\n') if k not in not_in_header]

with open('train/_no_tone/labelsForCMD.txt') as f:
    global_syllables = f.read().split(',')
    non_recorded = [syllable for syllable in valid_syllables if syllable not in global_syllables]
# the path name
_clip_name = '_clip_0.8'
_total_path = '/home/dmcl/boyu/update_0815/'

# the types of syllable
_top_name = '_1_desnet'
_mid_name = '_2_desnet'
_but_name = '_3_desnet'

_suffix = '_10000'

# the graph paths
graph_top = _total_path + _clip_name + '/' + _top_name + _suffix + '/' + _top_name + '.pb'
graph_mid = _total_path + _clip_name + '/' + _mid_name + _suffix + '/' + _mid_name + '.pb'
graph_but = _total_path + _clip_name + '/' + _but_name + _suffix + '/' + _but_name + '.pb'

# the labels paths
labels_top = _total_path + _clip_name + '/' + _top_name + _suffix + '/cmds/conv_labels.txt'
labels_mid = _total_path + _clip_name + '/' + _mid_name + _suffix + '/cmds/conv_labels.txt'
labels_but = _total_path + _clip_name + '/' + _but_name + _suffix + '/cmds/conv_labels.txt'

input_name = 'wav_data:0'
output_name = 'labels_softmax:0'

_parts = {'top': 0, 'mid': 1, 'but': 2}


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# create 3 types of graph.
#
# mode  : top, mid(middle) or but(button)
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def open_graph(mode):
    reset_graph()
    _graph_path = {'top': graph_top, 'mid': graph_mid, 'but': graph_but}
    lw.graph_only(_graph_path[mode])


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Reset the Graph, this can avoid the graph confilt.
#
# [Important]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def reset_graph():
    tf.reset_default_graph()


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# log10()
#
# [Math]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def log(value):
    return math.log10(value)


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Reset the Graph, this can avoid the graph conflit.
#
# wavs      : [ wav1_path , wav2_path ... ]
# how_many  : top number
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def word_creating(wavs, how_many):
    _labels_path = {'top': labels_top, 'mid': labels_mid, 'but': labels_but}

    # initial parameters
    syllable_lists = [[], [], []]
    score_lists = [[], [], []]

    # create
    for wav in wavs:
        print('[target]' + wav)
        # 3 parts single syllable
        for mode in ['top', 'mid', 'but']:
            open_graph(mode)
            syllable_list, score_list = lw.label_wav_modify(wav, _labels_path[mode],
                                                            input_name, output_name, how_many)
            syllable_lists[_parts[mode]].append(syllable_list)
            score_lists[_parts[mode]].append(score_list)

    return syllable_lists, score_lists


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# create the syllables to prepare making sentence.
#
#
# wavs      : [ wav1_path , wav2_path ... ]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def creating_syllables(wavs, number=3, enable=False):
    syllable_lists, score_lists = word_creating(wavs, number)
    syllables = []
    packets = zip(syllable_lists[_parts['top']], score_lists[_parts['top']],
                  syllable_lists[_parts['mid']], score_lists[_parts['mid']],
                  syllable_lists[_parts['but']], score_lists[_parts['but']])

    # create all possible syllables and make a sequence
    for packet in packets:
        # find out all syllable
        possible_syllables = \
            constructing_syllable(packet, number, enable)
        print(possible_syllables)

        # if in global_syllables
        syllables.append({syllable: score for syllable, score in possible_syllables})

    return log_to_prob(syllables)


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# combine the top, middle, button single syllable to the
# possible syllable. if is 'no_label' that means no single syllable.
#
# top_l, mid_l, but_l : single syllable list ['a', 'b', 'c']
# top_s, mid_s, but_s : single syllable score list [a_score, b_score, c_score]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def constructing_syllable(packet, number=3, enable=False):
    top_l, top_s, mid_l, mid_s, but_l, but_s = packet
    import re
    all_possible_syllable = []
    all_possible_syllable_non_recorded = []

    for x in range(number):
        for y in range(number):
            for z in range(number):
                top_t = top_l[x]
                mid_t = mid_l[y]
                but_t = but_l[z]

                # using log instead of operator 'add'
                total_score = math.fabs(log(top_s[x] * mid_s[y] * but_s[z]))
                syllable = re.sub('no_label', '', top_t + mid_t + but_t)

                if syllable in global_syllables:
                    all_possible_syllable.append([syllable, total_score])
                elif syllable in non_recorded:
                    all_possible_syllable_non_recorded.append([syllable, total_score])

    if enable:
        # the following one line indicates the results contain non-recorded valid syllable
        all_possible_syllable.extend(all_possible_syllable_non_recorded)

    # prevent the null of syllables
    if len(all_possible_syllable) == 0:
        all_possible_syllable.append(['ㄨㄛ', 0.000])

    return all_possible_syllable


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# call API function that we give wavs lists and produce the sentence list.
#
# enable indicates to contain non-recorded syllables
# wavs      : [ wav1_path , wav2_path ... ]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def syllables_to_sentence(wavs, number=3, enable=False):
    possible_lists = creating_syllables(wavs, number, enable)
    # print(possible_lists)

    sentence = mks.parseSentece_v1(possible_lists)

    return possible_lists, sentence


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
#
#  8/3
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def log_to_prob(lists_):
    possible_lists = []

    for temp_dict in lists_:
        sum_log = sum(list(temp_dict.values()))
        count_log = len(temp_dict.items())
        result = {k: round((sum_log - v) / (sum_log * (count_log - 1)), 6)
        if count_log > 1 else 1.000
                  for k, v in temp_dict.items()}

        possible_lists.append(result)

    return possible_lists


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# call API function that we give wavs lists and produce the sentence list.
#
# wavs      : [ wav1_path , wav2_path ... ]
# 7/26
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def syllables_convert(wavs, number=3):
    possible_lists = creating_syllables(wavs, number)

    return possible_lists


# words     : the list of syllables
#       [{syllable:score , syllable:score , syllable:score },
#        {syllable:score , syllable:score , syllable:score },
#                           ......
#        {syllable:score , syllable:score , syllable:score },
#        {syllable:score , syllable:score , syllable:score }]
# sorted_list = [(k, possible_dict[k]) for k in sorted(possible_dict, key=possible_dict.get, reverse=True)]


def syllable_sequence(possible_lists):
    return_lists = list()
    for syllables in possible_lists:
        sorted_list = [(k, syllables[k]) for k in sorted(syllables, key=syllables.get, reverse=True)]
        # [(, ),(, )...(, )]
        temp_list = list()
        for syllable in sorted_list:
            temp_list.append(syllable[0])
        return_lists.append(temp_list)

    return return_lists


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# call API function that we give wavs lists and produce the sentence list.
#
# wavs      : [ wav1_path , wav2_path ... ]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def syllables_to_sentence_OrgCNN(wavs):
    graph = "/home/dmcl/boyu/update_0721/_clip_0.8/dense/dense.pb"
    labels = "/home/dmcl/boyu/update_0721/_clip_0.8/dense/cmds/conv_labels.txt"
    reset_graph()
    lw.graph_only(graph)
    possible_lists = list()

    for wav in wavs:
        temp_top, score_top = lw.label_wav_modify(wav, labels, input_name, output_name, 5)
        temp_dict = dict()
        for i in range(5):
            if temp_top[i] != "_silence_" and temp_top[i] != "_unknown_":
                temp_dict.update({temp_top[i]: score_top[i]})
        possible_lists.append(temp_dict)

    sentence = mks.parseSentece_v1(possible_lists)

    return possible_lists, sentence


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# extract words without score
# sentence  : ['a', 'b', 'c'...]
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def extract_sentence(sentence):
    new_sentence = []
    length = len(sentence)
    for i in range(1, length):
        new_sentence.append(sentence[i])
    return new_sentence


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# sentence : ['我', '的', '想', '法', '是']
#                   |   (convert)
#                   V
# syllable : ['ㄨㄛ', 'ㄉㄜ', 'ㄒㄧㄤ', 'ㄈㄚ', 'ㄕ']
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def find_sentence_of_syllable(possible_lists, sentence):
    output_syllables = list()

    count = 0
    for word in sentence:
        syllables = pinyin(word, heteronym=True, style=Style.BOPOMOFO)
        syllables = syllables[0]
        ckeck_syll = possible_lists[count]
        for syllable in syllables:
            syllable = syllable.strip('ˊˋˇ˙')
            if syllable in ckeck_syll:
                output_syllables.append(syllable)
                break
        count = count + 1

    return output_syllables


# - - - - - - - - - - - - - - - - - - - - - - - - - - -
# main function
#
# - - - - - - - - - - - - - - - - - - - - - - - - - - -
def main(_):
    wavs_name = ['ㄨㄛ', 'ㄉㄜ', 'ㄒㄧㄤ', 'ㄈㄚ', 'ㄕ']
    wavs = take_audio.multi_syllables(wavs_name)
    possible_lists, sentence = syllables_to_sentence(wavs)
    print("==========================")
    print(possible_lists)
    print("==========================")
    print(sentence)
    """
    possible_lists = syllables_convert(wavs, 3)
    print(possible_lists)
    possible_lists_ = log_to_prob(possible_lists)
    print(possible_lists_)

    """
    input_sentence = ['我', '的', '想', '法', '是']
    new_sentence = extract_sentence(sentence)
    _bool = adjust_relation.adjustment(new_sentence, input_sentence)
    print(_bool)


if __name__ == '__main__':
    tf.app.run(main=main)
