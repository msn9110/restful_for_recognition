#!flask/bin/python
from flask import Flask, jsonify, request
import json

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    content = request.get_json().get('content')
    print(content)
    print(jsonify({'response': {'success': True, 'result': ['t', 'f']}}))
    return jsonify({'response': {'success': True, 'result': ['t', 'f']}})


if __name__ == '__main__':
    print({"response" : {"success":True, "result":['t','f']}})
    print(json.dumps({'response' : {'success':True, 'result':['t','f']}}))

    app.run(host='localhost', port=5000, debug=True)
