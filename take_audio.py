from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import glob
from random import *

import tensorflow as tf

global_path = "/home/dmcl/dataset/test/20180606/_clip_0.8/"
#global_path = '/home/dmcl/dataset/strem-test/'
#global_path = "/home/dmcl/dataset/sentence/我的想法是/"

def randomPath(wav_name):
    path = global_path + wav_name + "/*"
    wavs = glob.glob(path)
    rand_int = sample(wavs,  1)
    wav_path = rand_int[0]
    return wav_path

def multi_syllables(wavs_lists):
    wavs_path = []
    for wav in wavs_lists:
        temp_wav = randomPath(wav)
        wavs_path.append(temp_wav)
    return wavs_path


if __name__ == '__main__':
    wavs = ['ㄋㄧ','ㄏㄠ','ㄅㄨ','ㄏㄠ']
    paths = multi_syllables(wavs)
    print(paths)