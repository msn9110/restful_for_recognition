#!flask/bin/python
from flask import Flask, Response, request
import json
from clip_wave import WaveClip
from clip_stream_wave import StreamClip
import os
import re
import shutil
import create_syllable as cs
import adjust_relation

duration = 0.8
durationStr = str(duration)
target_dir = 'uploads/'
clipPath = target_dir + '_clip_' + durationStr + '/'
app = Flask(__name__)


@app.route('/recognize', methods=['GET', 'POST'])
def recognize():
    response = {"response": {"success": 0, 'uploaded': False}}
    data = request.get_json().get('data')
    label = data['label']  # may be tmp
    my_dir = target_dir
    if len(re.sub('[\u4e00-\u9fa5]+', '', label)) == 0:
        # label is sentence
        my_dir += 'sentence/'
    filename, raw = data['filename'], data['raw']

    if 'sentence' not in my_dir:
        # indicate is tone label
        if re.match('[0-4]-', filename) is None:
            my_dir += 'train'
        else:
            my_dir += 'test'

    my_dir += '/' + label
    os.makedirs(my_dir, exist_ok=True)
    path = my_dir + '/' + filename
    # write wave from json
    with open(path, 'wb') as f:
        f.write(bytes(data['raw']))
    if os.path.exists(path):
        wavefiles = []
        if not label == 'tmp' and 'sentence' not in my_dir:
            clip = WaveClip(path)
            output_dir = clipPath + label
            os.makedirs(output_dir, exist_ok=True)
            output_path = output_dir + '/clip-' + filename
            wavefiles.append(clip.clipWave_toFile(output_path, duration_sec=duration))
        else:
            s_clip = StreamClip(path)
            if label == 'tmp':
                _, files = s_clip.clipWave_toFile('', clipPath.strip('/'), True)
            else:
                _, files = s_clip.clipWave_toFile(label, clipPath.strip('/'))
            wavefiles.extend(files)

        sentence = None
        recognition_results = None
        used_indexes = None
        if len(wavefiles) > 0:
            try:
                possible_lists, sentence_rec = cs.syllables_to_sentence(wavefiles, 4, enable=True)
                sentence = ''
                for ch in sentence_rec[1:]:
                    sentence += ch
                used_syllables = cs.find_sentence_of_syllable(possible_lists, sentence)
                recognition_results = [[key for (key, score) in sorted(possible_list.items(),
                                                                       key=lambda item: item[1],
                                                                       reverse=True)]
                                       for possible_list in possible_lists]
                used_indexes = [recognition_results[i].index(used_syllables[i]) + 1
                                for i in range(len(possible_lists))]
            except:
                print('recognition exception')

        else:
            os.remove(path)
        response = {"response": {"success": len(wavefiles), "result_lists": recognition_results,
                                 "sentence": sentence, "usedIndexes": used_indexes,
                                 "uploaded": True}}

    return Response(json.dumps(response), mimetype='application/json')


@app.route('/updates', methods=['PUT'])
def updates():
    data = request.get_json()
    print(data)
    stream_files_move = data['streamFilesMove']
    syllable_files_move = data['syllableFilesMove']
    syllable_moves = len(syllable_files_move) > 0

    response = {}
    for rec in stream_files_move:
        filename = tuple(rec)[0]
        item = rec[filename]
        original = item['original']
        modified = item['modified']
        adjust_res = adjust_relation.adjustment(original, modified)
        print('adjust', adjust_res)

        src = target_dir + 'tmp/sentence/' + filename
        dst_dir = target_dir + 'sentence/' + modified
        os.makedirs(dst_dir, exist_ok=True)
        dst = dst_dir + '/' + filename
        flag = False
        if os.path.exists(src):
            shutil.move(src, dst)
            flag = True
        response[filename] = flag

    for rec in syllable_files_move:
        filename = tuple(rec)[0]
        item = rec[filename]
        label = item['label']
        tone = item['tone']

        src = clipPath + 'tmp/' + filename
        dst_dir = clipPath + label
        os.makedirs(dst_dir, exist_ok=True)
        origin = re.sub('(^clip-stream\d+-)', '', filename)
        dst = dst_dir + '/' + re.sub(origin + '$', '', filename) + tone + '-' + origin
        flag = False
        if os.path.exists(src):
            shutil.move(src, dst)
            flag = True
        syllable_moves = syllable_moves and flag

    response = {"success": True, "movedFilesState": response}
    return Response(json.dumps(response), mimetype='application/json')


@app.route('/remove', methods=['DELETE', 'PUT'])
def remove():
    data = request.get_json()
    # label = data['label']
    filename = data['filename']
    rm_cmd = 'find ' + target_dir + ' -name *' + filename + ' -exec rm {} \;'
    os.system(rm_cmd)
    response = {"success": True}
    return Response(json.dumps(response), mimetype='application/json')


if __name__ == '__main__':
    app.run(host='120.126.145.113', port=5000, debug=True)
