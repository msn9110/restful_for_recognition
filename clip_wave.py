import numpy as np
import wave
import re
import os
import filter as myfilter

class WaveClip:
    def __init__(self, path, toFilter=False):
        self.path = path
        spiltArr = str(path).split('/')
        self.name = spiltArr[len(spiltArr) - 1]
        #print(self.name)
        # 打开WAV文档
        f = wave.open(path, "rb")
        # 读取格式信息
        # (nchannels, sampwidth, framerate, nframes, comptype, compname)
        params = f.getparams()
        self.params = params
        self.nchannels, self.sampwidth, self.framerate, self.nframes = params[:4]

        # 读取波形数据
        str_data = f.readframes(self.nframes)
        f.close()

        # 将波形数据转换为数组
        wave_data = np.fromstring(str_data, np.int16)
        wave_data.shape = -1, 1
        self.wave_data = wave_data.T[0]
        if toFilter:
            lowcut, highcut = 100, 3500
            self.wave_data = myfilter.butter_bandpass_filter(self.wave_data,lowcut,highcut,self.framerate,order=6)

    def randomClip(self, duration_sec=1.0):
        wave_data = self.wave_data
        duration_size = int(self.framerate * duration_sec)
        if len(wave_data) <= duration_size:
            return np.append(wave_data, np.random.randint(-32, 33, size=duration_size - len(wave_data), dtype=np.int16))
        else:
            max_num = self.nframes - duration_size
            offset = np.random.randint(0, max_num)
            return wave_data[offset:offset + duration_size]

    def getparams(self):
        return self.params

    def clipWithNoise(self, noiseDir, duration_sec=1.0, shift_size=10, scale=1):
        clip_wave = clipWave(self.wave_data, self.framerate, duration_sec, shift_size)
        tmp = np.array(clip_wave, dtype=np.int32)
        files = list(filter(lambda file: file if file.endswith('.wav') else None, os.listdir(noiseDir)))
        num = np.random.randint(0, len(files))
        noisePath = noiseDir + '/' + files[num]
        noiseWave = WaveClip(noisePath)
        noiseClip = noiseWave.randomClip(duration_sec)
        tmp = tmp + scale*np.array(noiseClip, dtype=np.int32)
        for point in tmp:
            if point > 32767:
                point = 32767
            elif point < -32767:
                point = -32767
        return np.array(tmp, dtype=np.int16)

    def clipWave_toFile(self, outputPath='', duration_sec=1.0, shift_size=10, addNoise=False):
        if len(outputPath) == 0:
            outputPath = re.sub(self.name + '$', '', os.path.abspath(self.path)) + \
                         'clip-' + self.name
        if addNoise:
            outputPath = re.sub('.wav$', '', outputPath) +'-with_noise.wav'
            clip_wave = self.clipWithNoise('./_background_noise_', duration_sec, shift_size)
        else:
            clip_wave = clipWave(self.wave_data, self.framerate, duration_sec, shift_size)
        outfile = wave.open(outputPath, 'wb')
        outfile.setparams(self.params)
        outfile.setnframes(len(clip_wave))
        outfile.writeframes(clip_wave)
        outfile.close()
        return os.path.abspath(outputPath)

def clipWave(wave_data, framerate, duration_sec=0.8, shift_size=10):
    duration_size = int(framerate * duration_sec)
    if len(wave_data) <= duration_size:
        size = duration_size - len(wave_data)
        timeVector = np.arange(0, size) / framerate
        scales = np.random.randint(-1000, 1001, size=5)
        signal = scales[0] * np.sin(2 * np.pi * np.random.randint(100, 200) * timeVector)
        for i in range(1, 5):
            if i % 2 == 0:
                signal += scales[i] * np.sin(2 * np.pi * np.random.randint(100, 200) * timeVector)
            else:
                signal += scales[i] * np.cos(2 * np.pi * np.random.randint(100, 200) * timeVector)
        return np.append(wave_data, np.array(signal, dtype=np.int16))
    else:
        max_abs_mean = 0.0
        offset = -1
        for i in range(0, len(wave_data) - duration_size, shift_size):
            current_abs_mean = np.mean(np.abs(wave_data[i:i + duration_size]))
            if current_abs_mean > max_abs_mean:
                max_abs_mean = current_abs_mean
                offset = i
        return wave_data[offset:offset + duration_size]