#!flask/bin/python
from flask import Flask, Response, request
import json
import os

targetDir = 'uploads/'
app = Flask(__name__)

@app.route('/recognize', methods=['POST'])
def recognize():
    response = {"response": {"success": False}}
    data = request.get_json().get('data')
    label = data['label'] # may be tmp
    myDir = targetDir + label
    filename, raw = data['filename'], data['raw']
    os.makedirs(myDir, exist_ok=True)
    path = myDir + '/' + filename
    # write wave from json
    with open(path, 'wb') as f:
        f.write(bytes(data['raw']))
    # -----------------------------
    # recognize block
    # -----------------------------
    return Response(json.dumps(response), mimetype='application/json')

if __name__ == '__main__':
    app.run(host='120.126.145.113', port=5000, debug=True)

'''
tmp表示還不確定的label,用於辨識,若為注音的話,就代表已經確定該檔案的label
會將檔案移動至該label檔案夾底下,用於蒐集語音資料
raw就是檔案的integer array,一個element就是一個byte（0-255）
因為java的byte是-128-127所以我轉成integer是byte&0xff
我不知道swift byte範圍，這你就要自己研究了，反正integer array element範圍要是0-255
filename format ：(tone-)yyyyMMdd-HHmmss.wav ex:(1-)20180615-221212.wav
tone表示注音符號音調0-4(輕聲至四聲）
{"data":{"filename":"xxx.wav","label":"tmp", "raw":[123,255,0,...]}}tmp表示還不確定的label,用於辨識,若為注音的話,就代表已經確定該檔案的label
會將檔案移動至該label檔案夾底下,用於蒐集語音資料
raw就是檔案的integer array,一個element就是一個byte（0-255）
因為java的byte是-128-127所以我轉成integer是byte&0xff
我不知道swift byte範圍，這你就要自己研究了，反正integer array element範圍要是0-255
filename format ：(tone-)yyyyMMdd-HHmmss.wav ex:(1-)20180615-221212.wav
tone表示注音符號音調0-4(輕聲至四聲）
{"data":{"filename":"xxx.wav","label":"tmp", "raw":[123,255,0,...]}}
之後還有response format 先可以transfer再告訴我
byte to int
'''