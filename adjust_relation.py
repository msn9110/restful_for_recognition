import json
import math
from pypinyin import pinyin, lazy_pinyin, Style

semantic_path = 'word_log.json'
header_path = 'header_list.json'

semantic_record_path = 'semantic_record.json'
header_record_path = 'header_record.json'

# open file that can read and write
def openFile(file_path):
    _file = open(file_path,'r')
    return _file

# get json from file
def parseJson(file):
    json_lists = json.load(file)
    file.close()
    return json_lists

# write the lists in file
def writeFile(lists ,json_name):
    file = open(json_name,'w')
    results_json = json.dumps(lists,ensure_ascii=False,indent=4)
    file.write(results_json)
    file.close()

# input_dict : {'a':12,'b':23,'c':14}
# return [('b',23),('c',14),('a',12)]
def sortedDict(input_dict):
    sorted_list = [(k, input_dict[k]) for k in sorted(input_dict, key=input_dict.get, reverse=True)]
    return sorted_list

def header_adjustment(word,header_list,record):
    syllable = pinyin(word, style=Style.BOPOMOFO)
    syllable_ = syllable[0][0].strip('ˊˋˇ˙')

    #{}
    if syllable_ in header_list:
        # sub_list = {'a':12,'b':34,'c':56 ... }
        sub_list = header_list[syllable_]

        if word in sub_list:
            # score = 12 (a_score)
            score = sub_list[word]
            # get now position
            sub_tuple = sortedDict(sub_list)
            index_ = sub_tuple.index((word,score))
            # parameter, position and count
            count = 1
            pos = 1

            if syllable_ in record:
                # r_word = {'a':[1,3]}
                r_word = record[syllable_]

                if word in r_word:
                    # record, position and count
                    count = r_word[word][0]
                    pos = r_word[word][1]

                    if pos == index_:
                        count = count+1
                    #if position is not sane
                    else:
                        pos = index_
                        count = 1
                    r_word.update({word:[count,pos]})

                else:
                    # if not in r_word, setup word and its count 1
                    r_word.update({word:[1,index_]})
                    pos = index_
                    count = 1

                record.update({syllable_:r_word})
               
            else:
                record.update({syllable_:{word:[1,index_]}})
                pos = index_

            # compute
            
            print("pos:"+str(pos))
            print("count:"+str(count))
            add_ = math.pow(pos+1,count)
            sub_ = math.pow(pos+1,count-1)
            if pos==0:
                add_ = 16
            sub_tuple[index_] = (word,sub_tuple[index_][1] + add_)
            for i in range(pos):
                temp_sub = sub_tuple[i][1] - sub_
                if temp_sub < 1:
                    temp_sub = 1
                sub_tuple[i] = (sub_tuple[i][0],temp_sub)
            sub_list = dict(sub_tuple)
            header_list.update({syllable_:sub_list}) 
        else:
            sub_list.update({word:1})
            header_list.update({syllable_:sub_list})
    else:
        sub_list = {}
        sub_list.update({word:1})
        header_list.update({syllable_:sub_list})

    return header_list,record

# word_prev and word
def words_adjustment_plus(word_prev,word,semantic_list,record):
    syllable = pinyin(word, style=Style.BOPOMOFO)
    syllable_ = syllable[0][0].strip('ˊˋˇ˙')
    sub_list = {}
    if word_prev in semantic_list:
        sub_list = semantic_list[word_prev]

        if syllable_ in sub_list:
            _count = sub_list[syllable_][0]
            _list = sub_list[syllable_][1]
            if word in _list:
                log_score = _list[word]
                word_score = math.pow(2,log_score)
                # record {'觀':{ㄕㄤ:{賞:2}}}
                record_list = {}
                if word_prev in record:
                    record_list = record[word_prev]

                    # {ㄕㄤ:{賞:2}}
                    syllable_to_word = {}
                    if syllable_ in record_list:
                        syllable_to_word = record_list[syllable_]
                        # {賞:2}
                        if word in syllable_to_word:
                            count = syllable_to_word[word]
                            if count < 7:
                                count = count + 1
                        else:
                            count = 1
                        # {賞:3}
                        syllable_to_word.update({word:count})
                        log_score = math.log(word_score + math.pow(2,count),2)
                    else :
                        syllable_to_word.update({word:1})
                        log_score = math.log(word_score + math.pow(2,1),2)

                    record_list.update({syllable_:syllable_to_word})
                else:
                    record_list.update({syllable_:{word:1}})
                    log_score = math.log(word_score + math.pow(2,1),2)

                record.update({word_prev:record_list})
                _list.update({word:log_score})

            else:
                _list.update({word:0.5})

            sub_list[syllable_][1] = _list
            sub_list.update({syllable_:[_count,_list]})
        else:
            sub_list.update({syllable_:[1,{word:0.5}]})
    else: 
        sub_list.update({syllable_:[1,{word:0.5}]})

    semantic_list.update({word_prev:sub_list})

    return semantic_list,record

def words_adjustment_sub(word_prev,word,semantic_list):
    syllable = pinyin(word, style=Style.BOPOMOFO)
    syllable_ = syllable[0][0].strip('ˊˋˇ˙')
    sub_list = {}

    if word_prev in semantic_list:
        sub_list = semantic_list[word_prev]
        temp = list()
        if syllable_ in sub_list:
            _count = sub_list[syllable_][0]
            _list = sub_list[syllable_][1]

            if word in _list:
                log_score = _list[word]
                word_score = math.pow(2,log_score)
                word_score = word_score - 2
                if word_score > 1:
                    log_score = math.log(word_score , 2)
                else:
                    log_score = 0.5
                _list.update({word:log_score})

            temp.append(_count)
            temp.append(_list)
            sub_list.update({syllable_:temp})
        semantic_list.update({word_prev:sub_list})

    return semantic_list



# orignal  [ '我', '想', '要', '向', '上', '街']
# modified [ '我', '想', '要', '先', '唱', '歌']
def adjustment(original_sentence,modified_sentence):
    word_relation_file = openFile(semantic_path)
    word_relation_list = parseJson(word_relation_file)

    word_header_file = openFile(header_path)
    word_header_list = parseJson(word_header_file)

    header_record_file = openFile(header_record_path)
    header_record = parseJson(header_record_file)

    semantic_record_file = openFile(semantic_record_path)
    semantic_record = parseJson(semantic_record_file)

    if len(original_sentence) != len(modified_sentence):
        return False
    else:
        if original_sentence[0] != modified_sentence[0]:
            new_header_list,new_header_record = header_adjustment(modified_sentence[0],word_header_list,header_record)
            writeFile(new_header_list ,header_path )
            writeFile(new_header_record ,header_record_path)

        length = len(original_sentence)
        for i in range(1,length):
            print(i)
            if original_sentence[i] != modified_sentence[i]:
                word_relation_list,semantic_record = words_adjustment_plus(modified_sentence[i-1],modified_sentence[i],word_relation_list,semantic_record)
                word_relation_list = words_adjustment_sub(original_sentence[i-1],original_sentence[i],word_relation_list)

        writeFile(word_relation_list ,semantic_path)
        writeFile(semantic_record ,semantic_record_path)
        
        return True








if __name__ == '__main__':
    """
    a = {

    "ㄏㄨㄣ": {
        "諢": 2,
        "昏": 43,
        "渾": 36,
        "餛": 2,
        "婚": 76,
        "魂": 52,
        "琿": 1,
        "混": 103,
        "葷": 5
        }
    }
    e = {}
    b = {'ㄏㄨㄣ': {'渾':[3,5]}}
    c,d = header_adjustment('渾',a ,b)
    print(c)
    print(d)
    """
    """
    a = {
        "基": {
        "ㄅㄧㄠ": [
            1,
            {
                "表": 0.5
            }
        ],
        "ㄉㄨ": [
            27,
            {
                "督": 4.700439718141093,
                "度": 0.5
            }
        ],
        "ㄓ": [
            8,
            {
                "指": 0.5,
                "址": 0.5,
                "值": 0.5,
                "質": 2.0,
                "酯": 0.5
            }
        ],
        "ㄊㄧ": [
            1,
            {
                "體": 0.5
            }
        ]
        }
        }
        
    a = {}
    #b = {'基':{'ㄓ':{'值': 2}}}
    b = {}
    c,d=words_adjustment('基','癡',a,b)
    print(c)
    print(d)
    """
    orignal  = [ '我', '想', '要', '向', '上', '街']
    modified = [ '我', '想', '要', '先', '唱', '歌']
    bool_ = adjustment(orignal,modified)
    print(bool_)

