import wave
import numpy as np
import os
import re
from pypinyin_ext.zhuyin import convert_to_zhuyin
from clip_wave import clipWave

class StreamClip:

    def __init__(self, path):
        self.path = path
        splitArr = str(path).split('/')
        self.name = splitArr[-1]
        # print(self.name)
        # 打开WAV文档
        f = wave.open(path, "rb")
        # 读取格式信息
        # (nchannels, sampwidth, framerate, nframes, comptype, compname)
        params = f.getparams()
        self.params = params
        self.nchannels, self.sampwidth, self.framerate, self.nframes = params[:4]

        # 读取波形数据
        str_data = f.readframes(self.nframes)
        f.close()

        # 将波形数据转换为数组
        wave_data = np.fromstring(str_data, np.int16)
        wave_data.shape = -1, 1
        self.wave_data = wave_data.T[0]


    def clipWave(self, duration_sec=0.8, absMeanThreshold=0.0,
                 scale = 1, baseFrameSec=0.1, debug=False):
        wave_data = list(self.wave_data)
        # arguments to affect clip performance
        myAbsMean = np.mean(np.abs(wave_data))
        AbsMeanThreshold = myAbsMean
        if 800.0 <= myAbsMean < 3000.0:
            AbsMeanThreshold = 700.0
        elif myAbsMean > 3000.0:
            AbsMeanThreshold -= (myAbsMean // 500) * 200.0
        else:
            AbsMeanThreshold = 500.0
        # Custom Threshold
        if absMeanThreshold > 0.0:
            AbsMeanThreshold = absMeanThreshold
        ######################################
        frameSec = baseFrameSec / scale
        frameSize = int(frameSec * self.framerate)
        frames = []
        frameActs = []
        frameAbsMeanChanges = [] # To improve clip
        previousFrameAbsMean = 0.0 # initial
        for i in range(0, self.nframes, frameSize):
            # last frame
            if i + frameSize >= self.nframes:
                frame = wave_data[i:]
            else:
                frame = wave_data[i:i + frameSize]
            frames.append(frame)
            absMean = np.mean(np.abs(frame))
            act = absMean >= AbsMeanThreshold
            frameActs.append(act)
            frameAbsMeanChanges.append(absMean - previousFrameAbsMean)
            previousFrameAbsMean = absMean
            if debug:
                print(i/self.framerate, absMean)


        NumOfFrames = len(frameActs)

        # Variable to merge frames into voice
        frameUsedTimes = [0] * NumOfFrames
        results = []
        voice = []
        frameCount = 0

        # Threshold definition
        # Minimum of number of voice's previous or next frame to reserve
        NumOfPrevious = 2 * scale
        NumOfNext = 2 * scale
        # threshold of frameCount of a voice to check if voice should add to the results
        FrameCountThreshold = NumOfPrevious + 1 * scale + NumOfNext

        # Control FLAG
        # To check if voice has achieved maximum amplitude of the voice
        # if yes, its absMeanChange will smaller than or equal to -1500
        # then set this flag to true
        isInDecayedState = False
        # if in decayed state and its absMeanChange lager than or equal
        # to AbsMeanThreshold set this flag to true
        earlyReset = False

        def includeFrame(pos, offset=0, MaxUsedCount=2):
            nonlocal voice, frameCount, frameUsedTimes
            index = pos + offset
            # avoid index out of range
            if 0 <= index < NumOfFrames:
                if frameUsedTimes[index] < MaxUsedCount:
                    frame = frames[index]
                    voice.extend(frame)
                    frameCount += 1
                    frameUsedTimes[index] += 1
            return voice, frameCount

        def findIndexOfEndFrame(pos, forward=True):
            offset = 1
            end = NumOfFrames
            if forward:
                offset = -1
                end = -1
            begin = pos + offset
            index = begin
            for i in range(begin, end, offset):
                shouldInclude = not frameActs[i]
                if shouldInclude and offset * frameAbsMeanChanges[i] <= 750:
                    index = i
                else:
                    break
            return index

        def addVoiceToResults(voice, earlyReset, flag):
            nonlocal results, FrameCountThreshold
            if flag:
                FrameCountThreshold -= NumOfNext
            else:
                if earlyReset:
                    FrameCountThreshold -= NumOfPrevious
                else:
                    pass

            if frameCount >= FrameCountThreshold:
                voice = clipWave(np.array(voice, dtype=np.int16), self.framerate,
                                 duration_sec=duration_sec)
                absMean = np.mean(np.abs(voice))
                if debug:
                    print(absMean)
                if absMean >= AbsMeanThreshold:
                    results.append(voice)

            # reset voice
            reset(flag)

        def reset(flag):
            nonlocal voice, frameCount, isInDecayedState, earlyReset, FrameCountThreshold
            voice = []
            frameCount = 0
            isInDecayedState = False

            if flag:
                FrameCountThreshold += NumOfNext
            else:
                if earlyReset:
                    FrameCountThreshold += NumOfPrevious
                    earlyReset = False
                else:
                    pass

        for i in range(NumOfFrames):
            flag = frameActs[i]
            # detect frames[i] should be included
            if flag:
                if isInDecayedState and frameAbsMeanChanges[i] >= 2 * AbsMeanThreshold:
                    if debug:
                        print('early')
                        print(len(results)+1, i)
                    addVoiceToResults(voice, earlyReset, flag)
                    earlyReset = True

                # include previous frame to prevent distortion
                if len(voice) == 0 and not earlyReset:
                    numOfPrevious = i - findIndexOfEndFrame(i, True)
                    for j in range(numOfPrevious, 0, -1):
                        voice, frameCount = includeFrame(i, -j)

                if not isInDecayedState and frameAbsMeanChanges[i] <= -1500:
                    isInDecayedState = True

                # voice include current frame
                voice, frameCount = includeFrame(i)
            else:
                # include next frame to prevent distortion
                if frameCount > 0:
                    numOfNext = findIndexOfEndFrame(i, False) - i
                    for j in range(0, numOfNext, 1):
                        voice, frameCount = includeFrame(i, j)

                    if debug:
                        print(len(results) + 1, i)
                    addVoiceToResults(voice, earlyReset, flag)

        return results


    def clipWave_toFile(self, msg, outputDir='.', force=False, debug=False):
        if len(outputDir) == 0:
            outputDir = '.'
        myClip = self.clipWave(debug=debug)
        print('system slice a wave into ', len(myClip), 'waves')
        zhuyin = convert_to_zhuyin(re.sub('[^\u4e00-\u9fa5]+', '', msg))
        resultPath = []
        getTonePrefix = lambda t: '1-' if len(t) == 0 else str('˙ ˊˇˋ'.index(t)) + '-'
        if len(zhuyin) == len(myClip) or force:
            for i in range(len(myClip)):
                myDir = outputDir
                tone = ''
                try:
                    label = re.sub('[˙ˊˇˋ]$', '', zhuyin[i][0])
                    tone = getTonePrefix(re.sub(label, '', zhuyin[i][0]))
                    myDir += '/' + label
                except:
                    myDir += '/tmp'
                os.makedirs(myDir, exist_ok=True)
                outputPath = myDir + '/' + 'clip-stream' + str(i + 1) + '-' + tone\
                             + self.name
                if not os.path.exists(outputPath):
                    outfile = wave.open(outputPath, 'wb')
                    outfile.setparams(self.params)
                    outfile.setnframes(len(myClip[i]))
                    outfile.writeframes(myClip[i])
                    outfile.close()
                resultPath.append(os.path.abspath(outputPath))
        return len(zhuyin), resultPath